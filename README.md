# README.md

## 2 Editing

### 2.1 Teks

#### 2.1.1 Bold / Italic / Underline

**Tulisan Bold**
_Tulisan Italic_
<u>Tulisan Underline</u>

#### 2.1.1 List

- List
    - Option
        - Sub Option
        - Sub Option
        - Sub Option
    - Option
    - Option

#### 2.1.1 Paragraph
<p>Baris paragraf</p>

#### 2.1.1 Coding
```
echo "penulisan coding 1 paragraf dengan ``` (triple thick)"
```
Ini inline code ``dengan double tick``

> Ini tulisan buat Quotes

### 2.2 Media

#### 2.1.1 Gambar
```
![Gambar 1](./Screenshoot/Screenshot_1549982236.png "Tooltip Gambar")
```

![Gambar 1](./images/Screenshot_1549982236.png "Tooltip Gambar")

atau 

```
<img src="./Screenshoot/Screenshot_1549982236.png"/>
```

<img src="./images/Screenshot_1549982236.png"/>

### 2.3 Link
Caranya :
```
[tulisan yang nampak](alamat linknya)
```
Hasilnya : [tulisan yang nampak](alamat linknya)

atau bisa juga dengan HTML

## Referensi .md
[http://agea.github.io/tutorial.md/](http://agea.github.io/tutorial.md/)
